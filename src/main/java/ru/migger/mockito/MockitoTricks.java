package ru.migger.mockito;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.stubbing.Stubber;
import ru.migger.mockito.fuunction.*;

import static org.mockito.Mockito.doAnswer;

@SuppressWarnings("unchecked")
public class MockitoTricks {
    <R, T> Stubber doFunction(OneArgumentFunction<R, T> function) {
        return doAnswer(invocationOnMock -> function.call((T) invocationOnMock.getArguments()[0]));
    }

    <R, T1, T2> Stubber doFunction(TwoArgumentFunction<R, T1, T2> function) {
        return doAnswer(invocationOnMock -> function.call(
                (T1) invocationOnMock.getArguments()[0],
                (T2) invocationOnMock.getArguments()[1]
        ));
    }

    <R, T1, T2, T3> Stubber doFunction(ThreeArgumentFunction<R, T1, T2, T3> function) {
        return doAnswer(invocationOnMock -> function.call(
                (T1) invocationOnMock.getArguments()[0],
                (T2) invocationOnMock.getArguments()[1],
                (T3) invocationOnMock.getArguments()[2]
        ));
    }

    <R, T1, T2, T3, T4, T5> Stubber doFunction(FiveArgumentFunction<R, T1, T2, T3, T4, T5> function) {
        return doAnswer(invocationOnMock -> function.call(
                (T1) invocationOnMock.getArguments()[0],
                (T2) invocationOnMock.getArguments()[1],
                (T3) invocationOnMock.getArguments()[2],
                (T4) invocationOnMock.getArguments()[3],
                (T5) invocationOnMock.getArguments()[4]
        ));
    }

    <R, T1, T2, T3, T4, T5, T6> Stubber doFunction(SixArgumentFunction<R, T1, T2, T3, T4, T5, T6> function) {
        return doAnswer(invocationOnMock -> function.call(
                (T1) invocationOnMock.getArguments()[0],
                (T2) invocationOnMock.getArguments()[1],
                (T3) invocationOnMock.getArguments()[2],
                (T4) invocationOnMock.getArguments()[3],
                (T5) invocationOnMock.getArguments()[4],
                (T6) invocationOnMock.getArguments()[5]
        ));
    }

    <R, T1, T2, T3, T4, T5, T6, T7> Stubber doFunction(SevenArgumentFunction<R, T1, T2, T3, T4, T5, T6, T7> function) {
        return doAnswer(invocationOnMock -> function.call(
                (T1) invocationOnMock.getArguments()[0],
                (T2) invocationOnMock.getArguments()[1],
                (T3) invocationOnMock.getArguments()[2],
                (T4) invocationOnMock.getArguments()[3],
                (T5) invocationOnMock.getArguments()[4],
                (T6) invocationOnMock.getArguments()[5],
                (T7) invocationOnMock.getArguments()[6]
        ));
    }
}
