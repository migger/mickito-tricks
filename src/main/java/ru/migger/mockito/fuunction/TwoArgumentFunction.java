package ru.migger.mockito.fuunction;

@FunctionalInterface
public interface TwoArgumentFunction<R, T1, T2> {
    R call(T1 param1, T2 param2);
}
