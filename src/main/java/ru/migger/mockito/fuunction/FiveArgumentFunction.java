package ru.migger.mockito.fuunction;

@FunctionalInterface
public interface FiveArgumentFunction<R, T1, T2, T3, T4, T5> {
    R call(T1 param1, T2 param2, T3 param3, T4 param4, T5 param5);
}
