package ru.migger.mockito.fuunction;

@FunctionalInterface
public interface OneArgumentFunction<R, T> {
    R call(T param1);
}
