package ru.migger.mockito.fuunction;

@FunctionalInterface
public interface SixArgumentFunction<R, T1, T2, T3, T4, T5, T6> {
    R call(T1 param1, T2 param2, T3 param3, T4 param4, T5 param5, T6 param6);
}
