package ru.migger.mockito.fuunction;

@FunctionalInterface
public interface ThreeArgumentFunction<R, T1, T2, T3> {
    R call(T1 param1, T2 param2, T3 param4);
}
